hi Folded ctermbg=242
set foldmethod=indent
set foldlevel=0
set number

syntax on
filetype indent plugin on
set tabstop=4
set shiftwidth=4
set expandtab

set omnifunc=syntaxcomplete#Complete

set exrc
set secure




